from math import sqrt


class Point(object):
    def __init__(self, (x, y)):
        self.x = x
        self.y = y

    def get_x(self):
        return int(self.x)

    def get_y(self):
        return int(self.y)

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def get_position(self):
        return int(self.x), int(self.y)

    def get_distance_to_point(self, point):
        return sqrt(pow((self.x - point.get_x()), 2) + pow((self.y - point.get_y()), 2))
