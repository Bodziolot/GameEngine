from tkFileDialog import askopenfilename

from GameObjects.Circle import Circle
from GameObjects.Platform import Platform
from GameObjects.Point import Point


class Load(object):
    def __init__(self, objects_in_game):
        self.objects_in_game = objects_in_game
        self.objects = []

    def load_objects(self):
        filename = askopenfilename(initialdir="/")
        if len(filename) == 0:
            return
        with open(filename, "r") as text_file:
            width, height = 0, 0
            for line in text_file:
                line = line.split(" ")
                line = line[:len(line) - 1]
                if line[0] == "c":
                    self.create_circle(line[1:])
                elif line[0] == "p":
                    self.create_platform(line[1:])
                elif line[0] == "w":
                    width, height = line[1:]
        self.objects_in_game.load_objects(self.objects)
        return self.objects_in_game, int(width), int(height)

    def create_circle(self, data):
        data = map(float, data)
        circle = Circle() \
            .set_position(data[0], data[1]) \
            .set_velocities(data[2], data[3]) \
            .set_radius(data[4]) \
            .set_mass(data[5]) \
            .set_color(data[6], data[7], data[8])
        self.objects.append(circle)

    def create_platform(self, data):
        data = map(float, data[:8]) + [data[8]]
        platform = Platform() \
            .set_start_point(Point((data[0], data[1]))) \
            .set_end_point(Point((data[2], data[3]))) \
            .set_width(data[4]) \
            .set_color(data[5], data[6], data[7]) \
            .set_modifier(data[8])
        self.objects.append(platform)
