import random
from _tkinter import TclError

import pygame

from GameObjects.Circle import Circle
from GameObjects.Platform import Platform
from GameObjects.Point import Point
from ObjectsInGame.ObjectsInGame import ObjectsInGame
from Window.Window import Window

window_width = 500
window_height = 500
default_fps = 60


def create_many_circles(objects_in_game):
    for i in range(00):
        radius = random.randint(10, 30)
        circle = Circle() \
            .set_position(random.randint(radius, window_width - radius),
                          random.randint(radius, window_height - radius)) \
            .set_velocities(random.randint(-5, 5), random.randint(-5, 5)) \
            .set_color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)) \
            .set_radius(radius)
        objects_in_game.add_circle_before_game(circle)


def create_objects(objects_in_game):
    circle = Circle() \
        .set_position(44, 75) \
        .set_velocities(0.001, -2) \
        .set_color(0, 0, 255) \
        .set_radius(20)
    objects_in_game.add_circle_before_game(circle)

    circle = Circle() \
        .set_position(74, 25) \
        .set_velocities(0.1, 5) \
        .set_color(0, 255, 0) \
        .set_radius(20)
    objects_in_game.add_circle_before_game(circle)

    platform = Platform() \
        .set_start_point(Point((50, 300))) \
        .set_end_point(Point((200, 300)))
    objects_in_game.add_platform_before_game(platform)

    platform = Platform() \
        .set_start_point(Point((300, 50))) \
        .set_end_point(Point((300, 200)))
    objects_in_game.add_platform_before_game(platform)

    platform = Platform() \
        .set_start_point(Point((50, 400))) \
        .set_end_point(Point((200, 500))) \
        .set_modifier("turbo")
    objects_in_game.add_platform_before_game(platform)

    platform = Platform() \
        .set_start_point(Point((20, 120))) \
        .set_end_point(Point((80, 150)))
    objects_in_game.add_platform_before_game(platform)


def draw_scene(screen, window, objects_in_game):
    modified_object_after = objects_in_game.get_modified_object_after()
    modified_object_before = objects_in_game.get_modified_object_before()
    if modified_object_after and modified_object_before:
        window.restore_default_fps()
        modified_object_before.clear(screen)
        objects_in_game.clear_vectors_if_needed(modified_object_before, window, screen)
        for covered_game_object in objects_in_game.get_objects():
            covered_game_object.draw(screen)
            objects_in_game.draw_vectors_if_needed(covered_game_object, window, screen)
        modified_object_after.draw(screen)
        objects_in_game.draw_vectors_if_needed(modified_object_after, window, screen)

    else:
        screen.fill((255, 255, 255))
        for game_object in objects_in_game.get_objects():
            game_object.draw(screen)
            if isinstance(game_object, Circle):
                game_object.draw_velocity_vector(screen) if window.get_show_velocity_vector_status() else None
                game_object.draw_velocities_vectors(screen) if window.get_show_velocities_vectors_status() else None


def make_actions_for_objects(circles, platforms, window):
    window.restore_default_fps()
    for c in circles:
        c.check_for_update_closest_circles(circles)
        c.check_for_collision()
        c.check_out_of_screen(window)
        for platform in platforms:
            c.bounce_from_platform(window, platform)
    for c in circles:
        c.move_by_velocity(window)


def game_engine_editor():
    window = Window(window_width, window_height, default_fps)
    window.set_y_acceleration(0.1)
    window.initiate_game_editor_window()
    root, screen, clock, playing, window_working = window.get_game_editor_window_information()
    screen.fill((255, 255, 255))

    objects_in_game = ObjectsInGame()
    # create_objects(objects_in_game)
    create_many_circles(objects_in_game)
    window.update_objects_in_game(objects_in_game)
    objects_in_game.start_game()
    while window_working:
        try:
            # print clock.get_fps()
            clock.tick(window.get_fps())
            window.show_actual_fps_on_screen(clock)
            window.update_objects_in_game(objects_in_game)
            circles = objects_in_game.get_circles()
            platforms = objects_in_game.get_platforms()
            playing = window.is_playing()

            if playing:
                make_actions_for_objects(circles, platforms, window)
            else:
                window.check_for_mouse_action()

            draw_scene(screen, window, objects_in_game)
            pygame.display.flip()
            root.update()
        except (pygame.error, TclError) as err:
            root, screen, clock, playing, window_working = window.get_game_editor_window_information()
            print(err)
    pygame.quit()


if __name__ == "__main__":
    game_engine_editor()
