import pygame

from GameObjects.Point import Point


class Platform(object):
    def __init__(self):
        self.start_point = None
        self.end_point = None
        self.width = 1
        self.modifier = "no-modifier"
        self.possible_modifiers = {"no-modifier": 0.9, "frictionless": 1, "turbo": 1.5, "half-friction": 0.5}
        self.color = (0, 0, 0)
        self.a = None
        self.b = None
        self.vertical = False
        self.horizontal = False

    def info(self):
        return """Platform info:
                    \rstart point: %d %d
                    \rend point: %d %d
                    \ry = %.2fx + %d
                    \rvertical = %s
                    \rhorizontal = %s
            """ % (self.start_point.get_x(),
                   self.start_point.get_y(),
                   self.end_point.get_x(),
                   self.end_point.get_y(),
                   self.a,
                   self.b,
                   ["True", "False"][self.vertical],
                   ["True", "False"][self.vertical])

    def set_start_point(self, point):
        self.start_point = point
        self.set_a_and_b_if_possible()
        return self

    def set_end_point(self, point):
        self.end_point = point
        self.set_a_and_b_if_possible()
        return self

    def set_width(self, width):
        self.width = width
        return self

    def set_modifier(self, behaviour):
        self.modifier = behaviour
        return self

    def set_color(self, r, g, b):
        self.color = (r, g, b)
        return self

    def get_color(self):
        return self.color

    def set_a_and_b_if_possible(self):
        if self.start_point and self.end_point and self.end_point.get_x() - self.start_point.get_x():
            self.vertical = False
            self.horizontal = False
            self.a = float((self.end_point.get_y() - self.start_point.get_y())) / \
                     (self.end_point.get_x() - self.start_point.get_x())
        else:
            self.vertical = True
            self.a = 9223372036854775807
        if self.a == 0:
            self.horizontal = True
        self.b = self.start_point.get_y() - self.a * self.start_point.get_x()

    def get_start_point(self):
        return self.start_point

    def get_end_point(self):
        return self.end_point

    def get_width(self):
        return self.width

    def get_behaviour(self):
        return self.modifier

    def get_a(self):
        return self.a

    def get_b(self):
        return self.b

    def get_max_x(self):
        return max(self.start_point.get_x(), self.end_point.get_x())

    def get_max_y(self):
        return max(self.start_point.get_y(), self.end_point.get_y())

    def get_min_x(self):
        return min(self.start_point.get_x(), self.end_point.get_x())

    def get_min_y(self):
        return min(self.start_point.get_y(), self.end_point.get_y())

    def get_modifier(self):
        return self.possible_modifiers[self.modifier]

    def has_behaviour(self):
        return self.modifier is not None

    def is_vertical(self):
        return self.vertical

    def is_horizontal(self):
        return self.horizontal

    # TODO fix this method
    def covered_by_mouse(self, mouse_position):
        max_y = max(self.start_point.get_y(), self.end_point.get_y())
        min_y = min(self.start_point.get_y(), self.end_point.get_y())
        max_x = max(self.start_point.get_x(), self.end_point.get_x())
        min_x = min(self.start_point.get_x(), self.end_point.get_x())
        if self.vertical:
            if abs(mouse_position.get_x() - self.start_point.get_x()) < 2 and max_y > mouse_position.get_y() > min_y:
                return True
            else:
                return False
        if self.horizontal:
            if abs(mouse_position.get_y() - self.start_point.get_y()) < 2 and max_x > mouse_position.get_x() > min_x:
                return True
            else:
                return False
        platform_contains_x = (mouse_position.get_y() - self.b) / self.a
        platform_contains_y = self.a * mouse_position.get_x() + self.b
        return mouse_position.get_distance_to_point(Point((platform_contains_x, platform_contains_y))) <= 2

    def draw(self, screen):
        pygame.draw.line(screen, self.color, self.get_start_point().get_position(), self.get_end_point().get_position(),
                         int(self.width))

    def clear(self, screen):
        pygame.draw.line(screen, (255, 255, 255), self.get_start_point().get_position(), self.get_end_point().get_position(),
                         int(self.width))

    def get_covers_objects(self, objects_in_game, window):
        covered_objects = []
        for game_object in objects_in_game:
            if isinstance(game_object, Platform):
                if self.cover_platform(game_object):
                    covered_objects.append(game_object)
            else:
                if game_object.is_on_platform(self) \
                        or (self.cover_platform(game_object.get_velocity_platform())
                            and window.get_show_velocity_vector_status()) \
                        or (self.cover_platform(game_object.get_x_velocity_platform())
                            or self.cover_platform(game_object.get_y_velocity_platform())) \
                        and window.get_show_velocities_vectors_status():
                    covered_objects.append(game_object)
        return covered_objects

    def get_copy(self):
        new_platform = Platform() \
            .set_start_point(self.get_start_point()) \
            .set_end_point(self.get_end_point())
        return new_platform

    def cover_platform(self, platform):
        a = self.get_start_point()
        b = self.get_end_point()
        c = platform.get_start_point()
        d = platform.get_end_point()

        v1 = self.cross_product(c, d, a)
        v2 = self.cross_product(c, d, b)
        v3 = self.cross_product(a, b, c)
        v4 = self.cross_product(a, b, d)

        if (v1 > 0 and v2 < 0 or v1 < 0 and v2 > 0) and (v3 > 0 and v4 < 0 or v3 < 0 and v4 > 0):
            return 1

        if v1 == 0 and self.is_end_overlaps(c, d, a):
            return 1
        if v2 == 0 and self.is_end_overlaps(c, d, b):
            return 1
        if v3 == 0 and self.is_end_overlaps(a, b, c):
            return 1
        if v4 == 0 and self.is_end_overlaps(a, b, d):
            return 1
        return 0

    def has_similar_x(self, platform):
        return self.is_vertical() \
               and platform.is_vertical() \
               and abs(self.get_max_x() - platform.get_max_x()) < 5 \
               and (self.get_max_y() > platform.get_min_y() and self.get_min_y() < platform.get_max_y())

    def has_similar_y(self, platform):
        return self.is_horizontal() \
               and platform.is_horizontal() \
               and abs(self.get_max_y() - platform.get_max_y()) < 5 \
               and (self.get_max_x() > platform.get_min_x() and self.get_min_x() < platform.get_max_x())

    @staticmethod
    def cross_product(x, y, z):
        x1 = z.get_x() - x.get_x()
        y1 = z.get_y() - x.get_y()
        x2 = y.get_x() - x.get_x()
        y2 = y.get_y() - x.get_y()
        return x1 * y2 - x2 * y1

    @staticmethod
    def is_end_overlaps(x, y, z):
        return min(x.get_x(), y.get_x()) <= z.get_x() <= max(x.get_x(), y.get_x()) and \
               min(x.get_y(), y.get_y()) <= z.get_y() <= max(x.get_y(), y.get_y())
