import os

import pygame
import tkinter as tk
from tkinter import Frame, Button, StringVar, OptionMenu, messagebox, Label, RAISED

from SaveAndLoadModule.Load import Load
from SaveAndLoadModule.Save import Save


class Window(object):
    def __init__(self, game_window_width, game_window_height, default_fps):
        self.window_title = "Circulus vitiosus"
        self.window_x = 5
        self.window_y = 5
        self.game_window_width = game_window_width
        self.game_window_height = game_window_height
        self.window_width = self.game_window_width if self.game_window_width >= 500 else 420
        self.window_height = self.game_window_height + 57 if self.game_window_height >= 500 else 557
        self.x_acceleration = 0
        self.y_acceleration = 0
        self.root = None
        self.frame = None
        self.screen = None
        self.clock = None
        self.objects_in_game = None
        self.play_pause_button_name = None
        self.show_hide_velocity_vector_button_name = None
        self.show_hide_velocities_vectors_button_name = None
        self.create_object_button_name = None
        self.delete_button_state = "raised"
        self.add_vector_for_circle_button_state = "raised"
        self.create_object_button = None
        self.delete_object_button = None
        self.add_vector_for_circle_button = None
        self.show_velocity_vector = False
        self.show_velocities_vectors = False
        self.window_working = None
        self.default_fps = default_fps
        self.fps = 60
        self.fps_to_print = None

    def get_fps(self):
        return self.fps

    def set_fps(self, fps):
        self.fps = fps

    def restore_default_fps(self):
        self.fps = self.default_fps

    def set_x_acceleration(self, x_acceleration):
        self.x_acceleration = x_acceleration

    def set_y_acceleration(self, y_acceleration):
        self.y_acceleration = y_acceleration

    def get_game_window_height(self):
        return self.game_window_height

    def get_game_window_width(self):
        return self.game_window_width

    def get_game_window_size(self):
        return self.game_window_width, self.game_window_height

    def get_y_acceleration(self):
        return self.y_acceleration

    def get_x_acceleration(self):
        return self.x_acceleration

    def initiate_tkinter(self):
        self.root = tk.Tk()
        self.root.resizable(0, 0)
        self.root.title(self.window_title)
        img = tk.PhotoImage(file='CV_icon.gif')
        self.root.call('wm', 'iconphoto', '.', img)
        self.root.geometry('%dx%d+%d+%d' % (self.window_width, self.window_height, self.window_x, self.window_y))
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.frame = Frame(self.root, width=self.window_width, height=self.window_height)
        self.frame.grid(row=2, columnspan=9999)
        self.root.update()

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.root.destroy()
            self.window_working = False

    def initiate_pygame(self):
        pygame.init()
        self.screen = pygame.display.set_mode([self.game_window_width, self.game_window_height])
        self.clock = pygame.time.Clock()

    def connect_two_window(self):
        os.environ['SDL_WINDOWID'] = str(self.frame.winfo_id())
        os.environ['SDL_VIDEODRIVER'] = 'windib'

    def update_options_button(self):
        self.create_object_button.config(state=("disabled", "active")[self.is_playing()])

    def create_options(self):
        self.create_object_button_name = StringVar()
        self.create_object_button_name.set("create object")
        self.create_object_button = OptionMenu(self.root, self.create_object_button_name, "create object",
                                               "create circle", "create platform")
        self.create_object_button.config(state=("active", "disabled")[self.is_playing()], width=12)
        self.create_object_button.grid(row=0, column=4)

    def create_buttons(self):
        self.fps_to_print = StringVar()
        self.fps_to_print.set(str(self.fps))
        self.play_pause_button_name = StringVar()
        self.play_pause_button_name.set("play")
        self.show_hide_velocity_vector_button_name = StringVar()
        self.show_hide_velocity_vector_button_name.set("show velocity vector")
        self.show_hide_velocities_vectors_button_name = StringVar()
        self.show_hide_velocities_vectors_button_name.set("show velocities vectors")

        fps_text_field = Label(self.root, textvariable=self.fps_to_print, relief=RAISED)
        fps_text_field.grid(row=1, column=0)
        show_hide_velocity_vector = Button(self.root, command=self.update_show_hide_velocity_vector_button_name,
                                           textvariable=self.show_hide_velocity_vector_button_name, width=18)
        show_hide_velocity_vector.grid(row=0, column=1)

        show_hide_velocities_vectors = Button(self.root, command=self.update_show_hide_velocities_vectors_button_name,
                                              textvariable=self.show_hide_velocities_vectors_button_name, width=18)
        show_hide_velocities_vectors.grid(row=1, column=1)

        play_pause_button = Button(self.root, command=self.update_play_pause_button_name,
                                   textvariable=self.play_pause_button_name, width=5)
        play_pause_button.grid(row=0, column=3)

        restart_button = Button(self.root, command=self.restart_game, text="restart", width=5)
        restart_button.grid(row=1, column=3)

        reset_button = Button(self.root, command=self.reset_game, text="reset", width=3)
        reset_button.grid(row=0, column=0)

        save_button = Button(self.root, command=self.save_game, text="save", width=5)
        save_button.grid(row=0, column=6)

        load_button = Button(self.root, command=self.load_game, text="load", width=5)
        load_button.grid(row=1, column=6)

        self.delete_object_button = Button(self.root, command=self.change_state_of_delete_button, text="delete",
                                           relief=self.delete_button_state, width=5)
        self.delete_object_button.grid(row=0, column=5)

        self.add_vector_for_circle_button = Button(self.root, command=self.change_state_of_add_vector_for_circle,
                                                   text="add vector for circle",
                                                   relief=self.add_vector_for_circle_button_state, width=15)
        self.add_vector_for_circle_button.grid(row=1, column=4)

    def update_buttons_state(self):
        if self.create_object_button_name.get() != "create object":
            self.cancel_sunken_buttons()

    def cancel_delete_button(self):
        self.delete_button_state = "raised"
        self.delete_object_button.config(relief=self.delete_button_state)

    def cancel_add_vector_button(self):
        self.add_vector_for_circle_button_state = "raised"
        self.add_vector_for_circle_button.config(relief=self.add_vector_for_circle_button_state)

    def cancel_sunken_buttons(self):
        self.cancel_delete_button()
        self.cancel_add_vector_button()

    def cancel_creating_objects(self):
        self.create_object_button_name.set("create object")

    def change_state_of_add_vector_for_circle(self):
        self.add_vector_for_circle_button_state = "sunken" if self.add_vector_for_circle_button_state == "raised" \
            else "raised"
        self.add_vector_for_circle_button.config(relief=self.add_vector_for_circle_button_state)
        self.cancel_creating_objects()
        self.cancel_delete_button()

    def change_state_of_delete_button(self):
        self.delete_button_state = "sunken" if self.delete_button_state == "raised" else "raised"
        self.delete_object_button.config(relief=self.delete_button_state)
        self.cancel_creating_objects()
        self.cancel_add_vector_button()

    def load_game(self):
        load = Load(self.objects_in_game)
        objects_in_game, self.game_window_width, self.game_window_height = load.load_objects()
        pygame.quit()
        self.root.destroy()
        self.initiate_game_editor_window()
        self.update_objects_in_game(objects_in_game)
        self.objects_in_game.start_game()
        self.cancel_sunken_buttons()

    def save_game(self):
        save = Save(self.objects_in_game)
        save.save_game(self.game_window_width, self.game_window_height)
        self.cancel_sunken_buttons()

    def restart_game(self):
        self.objects_in_game.start_game()
        self.cancel_sunken_buttons()

    def reset_game(self):
        self.objects_in_game.reset_objects()
        self.cancel_sunken_buttons()

    def update_play_pause_button_name(self):
        self.update_options_button()
        if self.is_playing():
            self.play_pause_button_name.set("play")
        else:
            self.play_pause_button_name.set("pause")
            self.cancel_sunken_buttons()
            self.create_object_button_name.set("create object")

    def update_show_hide_velocity_vector_button_name(self):
        if self.show_hide_velocity_vector_button_name.get() == "hide velocity vector":
            self.show_hide_velocity_vector_button_name.set("show velocity vector")
            self.show_velocity_vector = False
        else:
            self.show_hide_velocity_vector_button_name.set("hide velocity vector")
            self.show_velocity_vector = True

    def update_show_hide_velocities_vectors_button_name(self):
        if self.show_hide_velocities_vectors_button_name.get() == "hide velocities vectors":
            self.show_hide_velocities_vectors_button_name.set("show velocities vectors")
            self.show_velocities_vectors = False
        else:
            self.show_hide_velocities_vectors_button_name.set("hide velocities vectors")
            self.show_velocities_vectors = True

    def get_show_velocity_vector_status(self):
        return self.show_velocity_vector

    def get_show_velocities_vectors_status(self):
        return self.show_velocities_vectors

    def update_objects_in_game(self, objects_in_game):
        self.objects_in_game = objects_in_game

    def create_menu(self):
        self.create_buttons()
        self.create_options()

    def initiate_game_editor_window(self):
        self.window_width = self.game_window_width if self.game_window_width >= 500 else 420
        self.window_height = self.game_window_height + 57 if self.game_window_height >= 500 else 557
        self.initiate_tkinter()
        self.create_menu()
        self.connect_two_window()
        self.initiate_pygame()
        self.window_working = True

    def get_game_editor_window_information(self):
        return self.root, self.screen, self.clock, self.is_playing(), self.is_window_working()

    def is_playing(self):
        return True if self.play_pause_button_name.get() == "pause" else False

    def is_window_working(self):
        return self.window_working

    def check_for_mouse_action(self):
        if pygame.mouse.get_pressed()[0] == 0:
            self.set_fps(5)
        self.update_buttons_state()
        if self.delete_button_state == "sunken":
            self.objects_in_game.delete_pressed_object(self)
        elif self.create_object_button_name.get() == "create circle":
            self.objects_in_game.create_circle()
        elif self.create_object_button_name.get() == "create platform":
            self.objects_in_game.create_platform()
        elif self.add_vector_for_circle_button_state == "sunken":
            self.objects_in_game.change_vector_for_circle()
        else:
            self.objects_in_game.check_for_move_circle()

    def show_actual_fps_on_screen(self, clock):
        self.fps_to_print.set(int(clock.get_fps()))
