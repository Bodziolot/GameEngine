import random

import pygame

from GameObjects.Circle import Circle
from GameObjects.Platform import Platform
from GameObjects.Point import Point


class ObjectsInGame(object):
    def __init__(self):
        self.objects = []
        self.objects_before_start = []
        self.objects_after_start = []
        self.modified_object_before = None
        self.modified_object_after = None
        self.position_set = False
        self.start_point_created = False
        self.end_point_created = False

    def get_modified_object_before(self):
        return self.modified_object_before

    def get_modified_object_after(self):
        return self.modified_object_after

    def set_modified_circle_after(self, modified_circle_after):
        self.modified_object_after = modified_circle_after

    def set_modified_circle_before(self, modified_circle_before):
        self.modified_object_before = modified_circle_before

    def add_circle(self, circle):
        self.objects_after_start.append(circle.get_copy())
        self.objects.append(circle)

    def add_platform(self, platform):
        self.objects_after_start.append(platform)
        self.objects.append(platform)

    def add_circle_before_game(self, circle):
        self.objects_before_start.append(circle.get_copy())

    def add_platform_before_game(self, platform):
        self.objects_before_start.append(platform)

    def get_circles(self):
        return filter(lambda game_object: isinstance(game_object, Circle), self.objects)

    def get_platforms(self):
        return filter(lambda game_object: isinstance(game_object, Platform), self.objects)

    def get_objects(self):
        return self.objects

    def reset_objects(self):
        self.objects_after_start = []
        self.objects = []
        for objects in self.objects_before_start:
            self.objects.append(objects.get_copy())

    def start_game(self):
        self.objects = []
        for obj in self.objects_before_start + self.objects_after_start:
            self.objects.append(obj.get_copy())

    def load_objects(self, objects):
        self.objects = objects
        self.objects_after_start = objects

    def draw_vectors_if_needed(self, game_object, window, screen):
        if isinstance(game_object, Circle):
            if window.get_show_velocity_vector_status() or game_object.has_modified_vector():
                game_object.draw_velocity_vector(screen)
            if window.get_show_velocities_vectors_status():
                game_object.draw_velocities_vectors(screen)

    def clear_vectors_if_needed(self, game_object, window, screen):
        if isinstance(game_object, Circle):
            if window.get_show_velocity_vector_status() or game_object.has_modified_vector():
                game_object.clear_velocity_vector(screen)
            if window.get_show_velocities_vectors_status():
                game_object.clear_velocities_vectors(screen)

    def reset_variables_to_create_new_platform_via_button(self):
        self.start_point_created = False
        self.end_point_created = False

    def get_special_position(self, mouse_position, platform):
        mods = pygame.key.get_mods()
        x_length = float(abs(platform.get_start_point().get_x() - mouse_position.get_x()))
        y_length = float(abs(platform.get_start_point().get_y() - mouse_position.get_y()))
        if mods & pygame.KMOD_SHIFT:
            if min(x_length, y_length) / max(x_length, y_length, 1) < 0.35:
                if x_length > y_length:
                    mouse_position.set_y(platform.get_start_point().get_y())
                else:
                    mouse_position.set_x(platform.get_start_point().get_x())
            else:
                if mouse_position.get_x() > platform.get_start_point().get_x():
                    if mouse_position.get_y() > platform.get_start_point().get_y():
                        mouse_position.set_y(platform.get_start_point().get_y() + (x_length + y_length) / 2)
                        mouse_position.set_x(platform.get_start_point().get_x() + (x_length + y_length) / 2)
                    else:
                        mouse_position.set_y(platform.get_start_point().get_y() - (x_length + y_length) / 2)
                        mouse_position.set_x(platform.get_start_point().get_x() + (x_length + y_length) / 2)
                else:
                    if mouse_position.get_y() > platform.get_start_point().get_y():
                        mouse_position.set_y(platform.get_start_point().get_y() + (x_length + y_length) / 2)
                        mouse_position.set_x(platform.get_start_point().get_x() - (x_length + y_length) / 2)
                    else:
                        mouse_position.set_y(platform.get_start_point().get_y() - (x_length + y_length) / 2)
                        mouse_position.set_x(platform.get_start_point().get_x() - (x_length + y_length) / 2)

    def delete_pressed_object(self, window):
        for _ in pygame.event.get():
            lmb_is_pressed = pygame.mouse.get_pressed()[0] == 1
            if lmb_is_pressed:
                window.restore_default_fps()
            mouse_position = Point(pygame.mouse.get_pos())
            temp_objects = []
            for obj in self.objects:
                if not lmb_is_pressed or not obj.covered_by_mouse(mouse_position):
                    temp_objects.append(obj)

            self.objects = temp_objects

    def create_circle(self):
        pygame.event.get()
        circle = self.get_modified_object_after() if self.get_modified_object_after() else Circle()
        lmb_is_pressed = pygame.mouse.get_pressed()[0] == 1
        if lmb_is_pressed and not self.position_set:
            x, y = pygame.mouse.get_pos()
            circle.set_position(x, y)
            self.set_modified_circle_before(circle)
            self.set_modified_circle_after(circle)
            self.position_set = True
        elif self.position_set and lmb_is_pressed:
            self.set_modified_circle_before(circle.get_copy())
            mouse_position = Point(pygame.mouse.get_pos())
            circle.set_color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            circle.set_radius(circle.get_distance_to_point(mouse_position))
        elif not lmb_is_pressed and self.position_set:
            circle = self.get_modified_object_after()
            self.add_circle(circle)
            self.position_set = False
            self.set_modified_circle_before(None)
            self.set_modified_circle_after(None)

    def create_platform(self):
        pygame.event.get()
        platform = self.get_modified_object_after() if self.get_modified_object_after() else Platform()
        lmb_is_pressed = pygame.mouse.get_pressed()[0] == 1
        mouse_position = Point(pygame.mouse.get_pos())
        if lmb_is_pressed and not self.start_point_created:
            self.set_modified_circle_before(platform)
            self.set_modified_circle_after(platform)
            platform.set_start_point(mouse_position)
            platform.set_end_point(mouse_position)
            self.start_point_created = True
        elif self.start_point_created and lmb_is_pressed:
            self.set_modified_circle_before(platform.get_copy())
            self.get_special_position(mouse_position, platform)
            platform.set_end_point(mouse_position)
        elif not lmb_is_pressed and self.start_point_created:
            self.add_platform(platform)
            self.reset_variables_to_create_new_platform_via_button()
            self.set_modified_circle_before(None)
            self.set_modified_circle_after(None)

    def change_vector_for_circle(self):
        pygame.event.get()
        lmb_is_pressed = pygame.mouse.get_pressed()[0] == 1
        mouse_position = Point(pygame.mouse.get_pos())
        if isinstance(self.modified_object_before, Circle) and isinstance(self.modified_object_after,
                                                                          Circle) and lmb_is_pressed:
            self.modified_object_before.set_the_same_velocities_as(self.modified_object_after)
            self.modified_object_after.set_velocities(
                (mouse_position.get_x() - self.modified_object_after.get_x()) / 10.,
                (mouse_position.get_y() - self.modified_object_after.get_y()) / 10.)
            return

        for circle in self.objects:
            if not isinstance(circle, Circle): continue
            if lmb_is_pressed and circle.covered_by_mouse(mouse_position):
                self.modified_object_before = circle.get_copy().change_modification_status()
                self.modified_object_after = circle.change_modification_status()
                self.modified_object_before.set_the_same_velocities_as(self.modified_object_after)
                self.modified_object_after.set_velocities(
                    (mouse_position.get_x() - self.modified_object_after.get_x()) / 10.,
                    (mouse_position.get_y() - self.modified_object_after.get_y()) / 10.)
            elif not lmb_is_pressed and self.modified_object_after:
                self.modified_object_before = None
                self.modified_object_after = self.modified_object_after.change_modification_status().get_none()

    def check_for_move_circle(self):
        pygame.event.get()
        lmb_is_pressed = pygame.mouse.get_pressed()[0] == 1
        mouse_position = Point(pygame.mouse.get_pos())
        if self.modified_object_before and lmb_is_pressed:
            self.modified_object_before.set_position(self.modified_object_after.get_x(),
                                                     self.modified_object_after.get_y())
            self.modified_object_after.set_position(mouse_position.get_x(), mouse_position.get_y())
            return

        for circle in self.objects:
            if not isinstance(circle, Circle): continue
            if lmb_is_pressed and circle.covered_by_mouse(mouse_position):
                self.modified_object_before = circle.get_copy()
                self.modified_object_after = circle
            elif not lmb_is_pressed and self.modified_object_after:
                self.modified_object_before = None
                self.modified_object_after = self.modified_object_after.get_none()
