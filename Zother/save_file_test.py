# from Tkinter import Tk
# from tkFileDialog import asksaveasfilename
#
# Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
# filename = asksaveasfilename()  # show an "Open" dialog box and return the path to the selected file
# print(filename)

import tkinter
from tkinter import ttk

root = tkinter.Tk()

style = ttk.Style()
style.map("C.TButton",
          foreground=[('pressed', 'red'), ('active', 'blue')],
          background=[('pressed', '!disabled', 'black'), ('active', 'white')]
          )

colored_btn = ttk.Button(text="Test", style="C.TButton").pack()

root.mainloop()
