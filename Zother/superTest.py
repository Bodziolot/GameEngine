class A(object):
    def __init__(self):
        print('A', '\n')


class B(object):
    def __init__(self):
        print('B', '\n')


class C(B, A):
    def __init__(self):
        super(C, self).__init__()
        B.__init__(self)


c = C()
