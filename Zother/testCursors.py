import Tkinter
from Tkinter import *

root = Tkinter.Tk()
root.config(cursor='gumby red red')
text = Tkinter.Text(root)
text.pack()
# oh no cursor is boring again! That makes sense, the default
# text cursor is slightly different than the root cursor
text.config(cursor='boat blue blue')  # phew!
B1 = Tkinter.Button(root, text="circle", relief=RAISED, \
                    cursor="circle")
B2 = Tkinter.Button(root, text="plus", relief=RAISED, \
                    cursor="plus")
B1.pack()
B2.pack()
root.mainloop()
