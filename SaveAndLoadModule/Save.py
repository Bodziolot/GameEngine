from tkFileDialog import asksaveasfilename


class Save(object):
    def __init__(self, objects_in_game):
        self.objects_in_game = objects_in_game
        self.circles = self.objects_in_game.get_circles()
        self.platforms = self.objects_in_game.get_platforms()

    def save_game(self, game_window_width, game_window_height):
        filename = asksaveasfilename(initialdir="/")
        if len(filename) == 0:
            return
        game_objects_string = ""
        temp_circle_string = ""
        for c in self.circles:
            temp_circle_string += "c " + str(c.get_x()) + " " + str(c.get_y()) + " "
            temp_circle_string += str(c.get_x_velocity()) + " " + str(c.get_y_velocity()) + " "
            temp_circle_string += str(c.get_mass()) + " " + str(c.get_radius()) + " "
            temp_circle_string += str(c.get_color()[0]) + " " + str(c.get_color()[1]) + " " + str(c.get_color()[2])
            game_objects_string += temp_circle_string + " ;\n"
            temp_circle_string = ""

        temp_platform_string = ""
        for p in self.platforms:
            temp_platform_string += "p " + str(p.get_start_point().get_x()) + " " + str(
                p.get_start_point().get_y()) + " "
            temp_platform_string += str(p.get_end_point().get_x()) + " " + str(p.get_end_point().get_y())
            temp_platform_string += " " + str(p.get_width()) + " "
            temp_platform_string += str(p.get_color()[0]) + " " + str(p.get_color()[1]) + " " + str(p.get_color()[2])
            temp_platform_string += " " + p.get_behaviour() + " ;\n"
            game_objects_string += temp_platform_string
            temp_platform_string = ""

        with open(filename + ".txt", "w") as text_file:
            text_file.write("w %d %d ;\n" % (game_window_width, game_window_height))
            text_file.write(game_objects_string)
