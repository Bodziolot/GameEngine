from math import sqrt

import pygame


class Figure(object):
    def __init__(self):
        self.x = 0
        self.y = 0
        self.x_velocity = 0
        self.y_velocity = 0
        self.x_acceleration = 0
        self.y_acceleration = 0
        self.color = 0, 0, 0
        self.mass = 0
        self.a = 0
        self.b = 0

    def info(self):
        return "\rx = %d \n\
                \ry = %d\n\
                \ry = %.2fx + %d\n\
                \rx_velocity = %d\n\
                \ry_velocity = %d\n\
                \rx_acceleration = %d\n\
                \ry_acceleration = %d\n\
                \rcolor = (%d, %d, %d)\n\
                \rmass = %d \n\
                " % \
               (self.x,
                self.y,
                self.a,
                self.b,
                self.x_velocity,
                self.y_velocity,
                self.x_acceleration,
                self.y_acceleration,
                self.color[0], self.color[1], self.color[2],
                self.mass)

    def set_position(self, x, y):
        self.x = x
        self.y = y
        return self

    def set_velocities(self, x_velocity, y_velocity):
        self.x_velocity = x_velocity
        self.y_velocity = y_velocity
        self.set_a_and_b_if_possible()
        return self

    def set_the_same_velocities_as(self, circle):
        self.x_velocity = circle.get_x_velocity()
        self.y_velocity = circle.get_y_velocity()
        self.set_a_and_b_if_possible()
        return self

    def set_color(self, r, g, b):
        self.color = r, g, b
        return self

    def set_mass(self, mass):
        self.mass = mass
        return self

    def set_a_and_b_if_possible(self):
        if self.x and self.y and self.x_velocity:
            self.a = float(self.y_velocity) / self.x_velocity
        else:
            self.a = 9223372036854775807
        self.b = self.y - self.a * self.x

    def get_position(self):
        return int(self.x), int(self.y)

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_x_velocity(self):
        return self.x_velocity

    def get_y_velocity(self):
        return self.y_velocity

    def get_velocity(self):
        return sqrt(pow(self.x_velocity, 2) + pow(self.y_velocity, 2))

    def get_x_acceleration(self):
        return self.x_acceleration

    def get_y_acceleration(self):
        return self.y_acceleration

    def get_mass(self):
        return self.mass

    def get_color(self):
        return self.color

    def move_by_velocity(self, window):
        self.add_window_accelerations(window)
        self.x += self.x_velocity
        self.y += self.y_velocity

    def add_window_accelerations(self, window):
        self.y_velocity += window.get_y_acceleration()
        self.x_velocity += window.get_x_acceleration()

    def draw_velocity_vector(self, screen):
        pygame.draw.line(screen, (0, 0, 0), self.get_position(),
                         (self.get_x() + self.get_x_velocity() * 10, self.get_y() + self.get_y_velocity() * 10))

    def clear_velocity_vector(self, screen):
        pygame.draw.line(screen, (255, 255, 255), self.get_position(),
                         (self.get_x() + self.get_x_velocity() * 10, self.get_y() + self.get_y_velocity() * 10))

    def draw_velocities_vectors(self, screen):
        pygame.draw.line(screen, (0, 0, 0), self.get_position(),
                         (self.get_x() + self.get_x_velocity() * 10, self.get_y()))
        pygame.draw.line(screen, (0, 0, 0), self.get_position(),
                         (self.get_x(), self.get_y() + self.get_y_velocity() * 10))

    def clear_velocities_vectors(self, screen):
        pygame.draw.line(screen, (255, 255, 255), self.get_position(),
                         (self.get_x() + self.get_x_velocity() * 10, self.get_y()))
        pygame.draw.line(screen, (255, 255, 255), self.get_position(),
                         (self.get_x(), self.get_y() + self.get_y_velocity() * 10))
