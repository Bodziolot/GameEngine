from math import sqrt, sin, cos, atan2, pi

import pygame

from GameObjects.Figure import Figure
from GameObjects.Platform import Platform
from GameObjects.Point import Point


class Circle(Figure):
    def __init__(self):
        super(Circle, self).__init__()
        self.radius = 0
        self.mass = self.radius
        self.closest_circles = []
        self.frames_since_last_update = 0
        self.max_frames_to_last_update = 60
        self.update_distance = 500
        self.modified_vector = False

    def info(self):
        return "\rx = %d \n\
                \ry = %d\n\
                \ry = %.2fx + %d\n\
                \rx_velocity = %d\n\
                \ry_velocity = %d\n\
                \rx_acceleration = %d\n\
                \ry_acceleration = %d\n\
                \rcolor = (%d, %d, %d)\n\
                \rmass = %d \n\
                \rradius = %d\n" % \
               (self.x,
                self.y,
                self.a,
                self.b,
                self.x_velocity,
                self.y_velocity,
                self.x_acceleration,
                self.y_acceleration,
                self.color[0], self.color[1], self.color[2],
                self.mass,
                self.radius)

    def set_radius(self, radius):
        self.radius = int(radius)
        self.mass = radius
        return self

    def get_radius(self):
        return self.radius

    def bounce_horizontally(self, platform=None, w_min=0, w_max=0):
        if self.x + self.x_velocity + self.radius >= w_max:
            self.x = w_max - self.radius
        elif self.x + self.x_velocity - self.radius <= w_min:
            self.x = w_min + self.radius

        platform_modifier = platform.get_modifier() if platform is not None else 0.7
        self.x_velocity = -self.x_velocity * platform_modifier
        self.y_velocity -= self.y_velocity * platform_modifier / 100.

    def bounce_vertically(self, platform=None, h_max=0, h_min=0):
        if self.y + self.y_velocity + self.radius >= h_max:
            self.y = h_max - self.radius
        elif self.y + self.y_velocity - self.radius <= h_min:
            self.y = h_min + self.radius

        platform_modifier = platform.get_modifier() if platform is not None else 0.7
        self.y_velocity = -self.y_velocity * platform_modifier
        self.x_velocity -= self.x_velocity * platform_modifier / 100.

    def get_distance_to_platform(self, platform):
        return abs(- platform.get_a() * self.x + self.y - platform.get_b()) / \
               sqrt(platform.get_a() * platform.get_a() + 1)

    def get_distance_between_centers(self, circle):
        return sqrt(pow((self.x - circle.get_x()), 2) + pow((self.y - circle.get_y()), 2))

    def get_distance_between_centers_in_next_step(self, c):
        return sqrt(pow((self.x + self.x_velocity - c.x - c.x_velocity), 2) +
                    pow((self.y + self.y_velocity - c.y - c.y_velocity), 2))

    def get_distance_to_point(self, point):
        return sqrt(pow((self.x - point.get_x()), 2) + pow((self.y - point.get_y()), 2))

    def covered_by_mouse(self, mouse_position):
        return self.get_distance_to_point(mouse_position) < self.get_radius()

    def check_out_of_screen(self, window):
        w, h = window.get_game_window_size()
        if self.x + self.x_velocity + self.radius > w or self.x + self.x_velocity - self.radius < 0:
            self.bounce_horizontally(w_max=w)
        elif self.y + self.y_velocity + self.radius > h or self.y + self.y_velocity - self.radius < 0:
            self.bounce_vertically(h_max=h)

    def make_collision(self, c):
        phi_r = atan2(c.get_y() - self.y, c.get_x() - self.x)
        phi = -phi_r if c.get_y() > self.y else phi_r
        t1 = atan2(self.y_velocity - self.y, self.x_velocity - self.x)
        t2 = atan2(c.get_y() - c.get_y_velocity(), c.get_x() - c.get_x_velocity())
        x_velocity = (((self.get_velocity() * cos(t1 - phi) * (self.mass - c.mass) + 2 * c.mass * c.get_velocity() *
                        cos(t2 - phi)) /
                       float(self.mass + c.mass)) * cos(phi)) - (self.get_velocity() * sin(t1 - phi) * sin(phi))
        y_velocity = (((self.get_velocity() * cos(t1 - phi) * (self.mass - c.mass) + 2 * c.mass * c.get_velocity() *
                        cos(t2 - phi)) /
                       float(self.mass + c.mass)) * sin(phi)) + (self.get_velocity() * sin(t1 - phi) * cos(phi))

        velocity_vector = sqrt(pow(x_velocity, 2) + pow(y_velocity, 2))
        self.x_velocity = -cos(phi_r) * velocity_vector
        self.y_velocity = -sin(phi_r) * velocity_vector

    def is_collision(self, circle):
        return self.get_distance_between_centers(circle) <= circle.get_radius() + self.get_radius()

    def is_not_itself(self, circle):
        return self != circle

    def is_not_on_the_same_position(self, circle):
        return self.x != circle.get_x() or self.y != circle.get_y()

    def check_for_collision(self):
        self.set_a_and_b_if_possible()
        for circle in self.closest_circles:
            if self.is_collision(circle) and self.is_not_itself(circle):
                self.make_collision(circle)

    def increase_frames_since_last_update(self):
        self.frames_since_last_update += 1
        if self.frames_since_last_update > self.max_frames_to_last_update:
            self.frames_since_last_update = 0

    def ready_for_update_closest_circles(self):
        self.increase_frames_since_last_update()
        return self.frames_since_last_update == 1

    def check_for_update_closest_circles(self, circles_list):
        if self.ready_for_update_closest_circles():
            self.update_closest_circles(circles_list)

    def update_closest_circles(self, circles_list):
        self.closest_circles = []
        for circle in circles_list:
            if self.get_distance_between_centers_in_next_step(circle) < self.update_distance:
                self.closest_circles.append(circle)

    def bounce_from_platform(self, window, platform):
        if self.is_on_platform(platform):
            if platform.is_vertical():
                if platform.get_end_point().get_x() < self.x:
                    min_value = platform.get_end_point().get_x() + platform.get_width() / 2
                    self.bounce_horizontally(platform, min_value, window.get_game_window_width())
                else:
                    max_value = platform.get_end_point().get_x() - platform.get_width() / 2
                    self.bounce_horizontally(platform, window.get_game_window_width(), max_value)
                return
            elif platform.is_horizontal():
                if platform.get_end_point().get_y() < self.y:
                    self.bounce_vertically(platform, window.get_game_window_height(), platform.get_end_point().get_y())
                else:
                    self.bounce_vertically(platform, platform.get_end_point().get_y(), window.get_game_window_height())
                return

            angle_of_incidence = abs((self.a - platform.get_a()) / (1 + self.a * platform.get_a()))
            angle_of_reflection = pi - angle_of_incidence
            velocity_vector = sqrt(pow(self.x_velocity, 2) + pow(self.y_velocity, 2))
            self.x_velocity = -cos(angle_of_reflection) * velocity_vector
            self.y_velocity = -sin(angle_of_reflection) * velocity_vector

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (int(self.x), int(self.y)), self.radius)

    def clear(self, screen):
        pygame.draw.circle(screen, (255, 255, 255), (int(self.x), int(self.y)), self.radius)

    def get_velocity_platform(self):
        return Platform().set_start_point(Point((self.x, self.y))) \
            .set_end_point(Point((self.x + self.x_velocity * 10, self.y + self.y_velocity * 10)))

    def get_x_velocity_platform(self):
        return Platform().set_start_point(Point((self.x, self.y))) \
            .set_end_point(Point((self.x + self.x_velocity * 10, self.y)))

    def get_y_velocity_platform(self):
        return Platform().set_start_point(Point((self.x, self.y))) \
            .set_end_point(Point((self.x, self.y + self.y_velocity * 10)))

    def collide_any_vectors(self, circle, covers_velocity_vector, covers_velocities_vectors):
        return (self.is_on_platform(circle.get_velocity_platform())
                and (covers_velocity_vector or circle.has_modified_vector())) \
               or (self.is_on_platform(circle.get_x_velocity_platform())
                   or self.is_on_platform(circle.get_y_velocity_platform())) and covers_velocities_vectors

    def any_vectors_collide_any_vectors(self, circle, covers_velocity_vector, covers_velocities_vectors):
        return (covers_velocity_vector and self.get_velocity_platform().cover_platform(circle.get_velocity_platform()))\
               or \
               (covers_velocities_vectors
                and (self.get_x_velocity_platform().has_similar_y(circle.get_x_velocity_platform())
                     or self.get_x_velocity_platform().cover_platform(circle.get_y_velocity_platform())
                     or self.get_y_velocity_platform().cover_platform(circle.get_x_velocity_platform())
                     or self.get_y_velocity_platform().has_similar_x(circle.get_y_velocity_platform()))) \
               or \
               (covers_velocities_vectors and covers_velocity_vector
                and (self.get_velocity_platform().cover_platform(circle.get_x_velocity_platform())
                     or self.get_velocity_platform().cover_platform(circle.get_y_velocity_platform())
                     or circle.get_velocity_platform().cover_platform(self.get_x_velocity_platform())
                     or circle.get_velocity_platform().cover_platform(self.get_y_velocity_platform())))

    def get_covers_objects(self, objects_in_game, window):
        covers_velocity_vector = window.get_show_velocity_vector_status()
        covers_velocities_vectors = window.get_show_velocities_vectors_status()
        covered_objects = []
        for game_object in objects_in_game:
            if isinstance(game_object, Circle):
                if (self.any_vectors_collide_any_vectors(game_object, covers_velocity_vector, covers_velocities_vectors)
                        or self.collide_any_vectors(game_object, covers_velocity_vector, covers_velocities_vectors)
                        or game_object.collide_any_vectors(self, covers_velocity_vector, covers_velocities_vectors)
                        or self.is_collision(game_object)) and self.is_not_on_the_same_position(game_object):
                    covered_objects.append(game_object)
            else:
                if self.is_on_platform(game_object):
                    covered_objects.append(game_object)
        return objects_in_game

    def is_on_platform(self, platform):
        return platform.get_max_x() + platform.get_width() / 2 > self.x - self.radius and \
               platform.get_min_x() - platform.get_width() / 2 < self.x + self.radius and \
               platform.get_max_y() + platform.get_width() / 2 > self.y - self.radius and \
               platform.get_min_y() - platform.get_width() / 2 < self.y + self.radius and \
               self.get_distance_to_platform(platform) < self.radius + platform.get_width() / 2

    def get_copy(self):
        new_circle = Circle() \
            .set_position(self.get_x(), self.get_y()) \
            .set_velocities(self.get_x_velocity(), self.get_y_velocity()) \
            .set_color(self.get_color()[0], self.get_color()[1], self.get_color()[2]) \
            .set_radius(self.get_radius())
        return new_circle

    def change_modification_status(self):
        self.modified_vector = [True, False][self.modified_vector]
        return self

    def get_none(self):
        return None

    def has_modified_vector(self):
        return self.modified_vector
